

import java.text.DecimalFormat;
import java.util.Random;

import weka.core.Instance;
import weka.core.Instances;

public class ID3tree {
	// to store the root of the tree we needa root of Node type
	private Node root;
	// since it has data - we store instances data
	private Instances treeData;

	public Node getRoot() {
		return root;
	}

	public void setRoot(Node root) {
		this.root = root;
	}

	public Instances getTreeData() {
		return treeData;
	}

	public void setTreeData(Instances treeData) {
		this.treeData = treeData;
	}

	// this is the Constructor for the Decision Tree
	public ID3tree(Instances train) {
		treeData = train;
		root = new Node();
		// we call the method to crate the tree here
		root.makeTree(treeData);
	}

	// this method is used for cross validation - it takes data and
	// number of folds as input
	public float crossValidate(Instances allData, int folds) {
		//Instances temp=allData;
		long seed= System.nanoTime();
		allData.randomize(new Random(seed));
		//System.out.println("folds : "+folds);
		Instances[] breaked= new Instances[folds];

		for(int i=0;i<folds;i++){
			//System.out.println(i);
			int size = treeData.size();
			breaked[i] = new Instances(treeData,size);
		}
		for(int i=0;i<(allData.size()-(treeData.numInstances()%folds));i++){
			Instance each = allData.get(i);
			breaked[i/(treeData.numInstances()/folds)].add(each);
		}
		//System.out.println("breaked.length : "+breaked.length);
		float accuracy= (float) calcAccuracy(breaked, allData, folds);
		return accuracy;
	}

	// this is a helper function 
	// it helps in calculating the accuracy
	private float calcAccuracy(Instances[] breaked, Instances allData, int folds){
		DecimalFormat df = new DecimalFormat("##0.00###");
		float accuracy=0;
		for(int i=0; i<folds;i++) {
			Instances testingData= breaked[i];
			//System.out.println("test : "+test.size());
			Instances trainindData= new Instances(allData, allData.size());
			float count=0;
			for(int j=0; j<folds; j++) {
				if(j!=i) {
					for(int k=0;k<breaked[j].size();k++) {
						trainindData.add(breaked[j].get(k));
					}
				}
			}
			ID3tree desion = new ID3tree(trainindData);
			for(int j=0; j<testingData.size();j++) {
				int value = desion.getRoot().findClass(testingData.get(j));
				if(value==testingData.get(j).classValue()){
					count++;
				}
			}
			float acc = count/testingData.size();
			accuracy= accuracy + acc;
		}

		accuracy= accuracy/folds;
		accuracy = 100*accuracy;
		System.out.println("Accuracy : "+df.format(accuracy)+" %");
		return accuracy;
	}

	public void setClass(Instance inst){
		int classIndex = root.findClass(inst);
		inst.setClassValue(classIndex);
	}	
}

// this class is used for storing a sub-tree information
// while processing the tree data
class SubTree {
	// this is the data
	private Instances subTreeData;
	// since we use this for partitioning based on a particular 
	// attribute - we need to store its index according to the
	// order in the input file
	private int attriIndex; 
	// gain for this particular set of data
	private float infoGain;
	// here we store the count of class values
	private int[] classCount;
	// we check here - using error pruning
	// if it is a correct partitioning attribute
	private boolean correct=true;
	// to store the number of sub parts
	private int numOfSubParts;  
	// if the attribute is numeric we need to store the point of division
	private float divisionPoint;   

	public Instances getSubTreeData() {
		return subTreeData;
	}

	public void setSubTreeData(Instances subTreeData) {
		this.subTreeData = subTreeData;
	}

	public int getAttriIndex() {
		return attriIndex;
	}

	public void setAttriIndex(int attriIndex) {
		this.attriIndex = attriIndex;
	}

	public float getInfoGain() {
		return infoGain;
	}

	public void setInfoGain(float infoGain) {
		this.infoGain = infoGain;
	}

	public int[] getClassCount() {
		return classCount;
	}

	public void setClassCount(int[] classCount) {
		this.classCount = classCount;
	}

	public boolean isValid() {
		return correct;
	}

	public void setValid(boolean isValid) {
		this.correct = isValid;
	}

	public int getNumOfSubParts() {
		return numOfSubParts;
	}

	public void setNumOfSubParts(int numOfSubParts) {
		this.numOfSubParts = numOfSubParts;
	}

	public float getDivisionPoint() {
		return divisionPoint;
	}

	public void setDivisionPoint(float divisionPoint) {
		this.divisionPoint = divisionPoint;
	}

	// this is the constructor of the sub tree
	// we provide it the data and attribute index
	public SubTree(Instances data,int index){
		//System.out.println("in cons");
		subTreeData = data;
		attriIndex = index;
		int classValues = countClassValues(subTreeData);
		//System.out.println("classValues : "+classValues);
		classCount = new int[classValues];

		// we count and store the class values for this sub set of data
		for(int i=0;i<data.numInstances();i++){
			int classVal = (int)data.instance(i).classValue();
			//System.out.println(classVal);
			//System.out.println(data.instance(i));
			classCount[classVal] = classCount[classVal]+1;
		}

		if(subTreeData.attribute(attriIndex).isNumeric()){
			numOfSubParts = 2; // as we do a binary split on numeric values
			float min = 10000;
			float max = -999999;

			for(int i=0;i<subTreeData.numInstances();i++){
				//System.out.println("subTreeData.instance(i).value(attriIndex) : "+subTreeData.instance(i).value(attriIndex));
				if(subTreeData.instance(i).value(attriIndex) < min){
					//System.out.println("in min ?");
					min = (float) subTreeData.instance(i).value(attriIndex);
				}
				if(max < subTreeData.instance(i).value(attriIndex)){
					//System.out.println("in max ?");
					max = (float) subTreeData.instance(i).value(attriIndex);
				}
			}
			//System.out.println("min : "+min);
			//System.out.println("max : "+max);
			// we use no. of intervals as 10
			infoGain = infoGain((float)10, max, min);
			//System.out.println("infoGain : "+infoGain);
		} else if(subTreeData.attribute(attriIndex).isNominal()){
			//System.out.println("in nomi");
			numOfSubParts = subTreeData.attribute(attriIndex).numValues();
			//System.out.println("numOfSubParts : "+numOfSubParts);
			infoGain = gain();
			//System.out.println("infoGain in func : "+infoGain);
		}

	}

	//This method is used to find the entropy of a single attribute
	//It is based on the simple formula of the entropy
	private float getEntropy(int[] count) {
		//System.out.println("count.len : "+count.length);
		float entropy=0;
		int total=0;
		for(int i=0; i<count.length;i++) {
			//System.out.println("count : "+count[i]);
			total = total + count[i];			
		}
		//System.out.println("total : "+total);

		for(int i=0; i<count.length;i++) {
			if(count[i]!=0) {
				//System.out.println("count[i] :"+count[i]+", total : "+total+", Math.log10(count[i]/total) : "+Math.log10((float)count[i]/total));
				//formula to calculate entropy
				entropy=  (float) (entropy - (((float)count[i]/total)*(Math.log10((float)count[i]/total)/(Math.log10(2)))));
				//System.out.println(" entropy : "+entropy);
			}
		}
		return entropy;
	}
	
	//This method is used to calculate the gain
	//This used for only numeric attributes
	private float infoGain(float sections, float low, float max) {
		float parentEntropy = (float) getEntropy(classCount);
		//System.out.println("parentEntropy : "+parentEntropy);

		//double partition=0;
		float gain=100000;
		//float distance= max-low;
		for(int i=0;i<sections-1;i++){
			float part=  (low+ (max-low)/sections*(i+1));
			float eachGain=  (float) partionedEntropy(part);
			//System.out.println("eachGain : "+eachGain);
			//Take the least one to maximise entropy
			if(eachGain < gain) {
				gain= eachGain;
				divisionPoint=part;
			}
		}
		//System.out.println("gain : "+gain);
		return (float) (parentEntropy - gain);
	}

	//This method is used to find the entropy of each individual partitions
	//This is used for numeric data
	private double partionedEntropy(double partition) {
		int low=0;
		int high=0;
		int[] lowCount= new int[subTreeData.classAttribute().numValues()];
		int[] highCount= new int[subTreeData.classAttribute().numValues()];

		for(int i=0;i<subTreeData.numInstances();i++){
			double temp = subTreeData.instance(i).value(attriIndex);

			if(temp<partition){
				lowCount[(int) subTreeData.instance(i).classValue()]++;
				low++;
			}else{
				highCount[(int) subTreeData.instance(i).classValue()]++;
				high++;
			}			
		}

		double finalEntropy=0;
		//calculate entropy of each parts
		double lowEnt= getEntropy(lowCount);
		double highEnt= getEntropy(highCount);

		finalEntropy= (lowEnt*low/subTreeData.numInstances())+highEnt*high/subTreeData.numInstances();
		return finalEntropy;
	}

	//This method calculates gain based on entropies
	//It subtracts parent entropy- child entropies
	private float gain() {
		//calculate gain of parent attributes
		float parentEntropy = getEntropy(classCount);
		//System.out.println("parentEntropy: "+parentEntropy);
		float[] allEntropies= new float[subTreeData.attribute(attriIndex).numValues()];	
		int[][] values = new int[subTreeData.attribute(attriIndex).numValues()][subTreeData.classAttribute().numValues()];
		for(int i=0; i<subTreeData.numInstances();i++) {
			values[(int)subTreeData.instance(i).value(attriIndex)][(int)subTreeData.instance(i).classValue()]++;
		}

		//calculate Entropies if each values
		allEntropies= levelEntropy(values);
		int totalweight=subTreeData.numInstances();
		int[] weight= new int[subTreeData.attribute(attriIndex).numValues()];

		for(int i=0; i<subTreeData.numInstances();i++) {
			weight[(int)subTreeData.instance(i).value(attriIndex)] ++;
		}
		//find gain
		float gain= getGain(parentEntropy, allEntropies, weight, totalweight);

		return gain;
	}

	//This method provides gain by subtracting entropies.
	private float getGain(float totalEntropy, float[] entropy, int[] weight, int totalweight) {
		float total=0;

		for(int i=0; i<entropy.length;i++) {
			total= total+((weight[i]/ totalweight)*entropy[i]);
		}
		return totalEntropy - total;
	}

	//This method calculates entroy of each leval or values of attribues
	//It takes a matrix values and class attributes
	private float[] levelEntropy(int[][] arr) {
		float[] allEntropies=new float[arr.length];
		//System.out.println(arr.length);
		for(int i=0; i<arr.length;i++) {
			int[] newArr= new int[arr[i].length];
			for(int j=0;j<arr[i].length;j++) {
				newArr[j]=arr[i][j];
			}
			allEntropies[i]=getEntropy(newArr);
		}
		return allEntropies;
	}

	// we count the number of class values
	private int countClassValues(Instances data) {
		return data.classAttribute().numValues();
	}

	// this method is used to split the data into multiple sub-sets based
	// on the type of value
	public Instances[] splitIntoSubTrees(Instances data) {
		Instances[] subTrees = new Instances[numOfSubParts];

		for(int i=0;i<numOfSubParts;i++){
			subTrees[i] = new Instances(data, data.numInstances());
		}

		for(int i=0;i<data.numInstances();i++){
			Instance inst = data.instance(i);
			int instState = getInstanceState(inst);
			subTrees[instState].add(inst);
		}

		return subTrees;
	}

	// this method is used while testing data
	// we use this method to determine the result class of a record
	public int getInstanceState(Instance inst) {
		if(inst.attribute(attriIndex).isNumeric()){
			float numericValue = (float) inst.value(attriIndex);
			if(numericValue < divisionPoint){
				return 1;
			}
		} else if(inst.attribute(attriIndex).isNominal()){
			int state = (int) inst.value(attriIndex);
			return state;
		}
		return 0;
	}

	// this method is used for error pruning technique
	// we check if the sub-set of data has more than the 
	// number of result classes - else we mark it as invalid 
	// and do not recurse on it further
	public boolean erroPruning(){
		if(subTreeData.attribute(attriIndex).isNumeric()){
			float [] classCount = new float[2];
			for(int i=0;i<subTreeData.numInstances();i++){
				double val = subTreeData.instance(i).value(attriIndex);
				if(val < divisionPoint){
					classCount[0]++;
				}else{
					classCount[1]++;
				}
			}
			for(int i=0;i<classCount.length;i++){
				if(classCount[i]<2){
					correct = false;
					return correct;
				}

			}
			correct = true;
		}else{
			int classValues = countClassValues(subTreeData);
			float [] classCount = new float[classValues];

			for(int i=0;i<subTreeData.numInstances();i++){
				int classVal = (int)subTreeData.instance(i).classValue();
				//System.out.println(classVal);
				//System.out.println(data.instance(i));
				classCount[classVal] = classCount[classVal]+1;
			}

			for(int i=0;i<classCount.length;i++){
				if(classCount[i]<classValues){
					correct = false;
					return correct;
				}
			}
			correct = true;
		}

		return correct; // return if the tree is a valid subtree
	}
}

