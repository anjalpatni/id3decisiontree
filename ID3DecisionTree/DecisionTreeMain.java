import java.util.Properties;

import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class DecisionTreeMain {
	public static void main(String[] args){
		try {
			// we use ID3DecisionTree.properties to read the names of the training 
			// and test data files
			Properties properties = new Properties();
			properties.load(DecisionTreeMain.class.getResourceAsStream("/ID3DecisionTree.properties"));
			
			String trainingDataSource = properties.getProperty("trainingData");
			String testDataSource = properties.getProperty("testData");
			
			// we read data into DataSource object and store it into instances
			DataSource trainingData = new DataSource(trainingDataSource);
			Instances train = trainingData.getDataSet();
			
			// if the class index is not defined we set it to be the last attribute
			if(train.classIndex() == -1){
				train.setClassIndex(train.numAttributes() -1);
			}
			
			// now we create a tree based on training data
			ID3tree tree = new ID3tree(train);
			
			System.out.println("Based on Cross-Validation using 10 folds :");
			tree.crossValidate(train, 10);
			
			// we read data into DataSource object and store it into instances
			DataSource testingData = new DataSource(testDataSource);
			Instances test = testingData.getDataSet();
			
			// if the class index is not defined we set it to be the last attribute
			if(test.classIndex() == -1){
				test.setClassIndex(test.numAttributes() -1);
			}
			
			// displaying results
			System.out.println();
			System.out.println("Test Data with ID3 Decision Tree Results :");
			for(int i=0; i< test.numInstances(); i++){
				tree.setClass(test.instance(i));
				System.out.println(test.get(i));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
