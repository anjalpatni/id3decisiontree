

import weka.core.Instance;
import weka.core.Instances;

public class Node {
	// a node can have many child nodes so we choose an array
	private Node[] childNodes; 
	// to know if this node is leaf node
	private boolean leafNode;
	// to store the result for this node
	private int result;
	// to store class counts of the variations
	private int[] classCount;
	// subTree 
	private SubTree subTree;
	
	public Node[] getChildNodes() {
		return childNodes;
	}
	public void setChildNodes(Node[] childNodes) {
		this.childNodes = childNodes;
	}
	public boolean isLeafNode() {
		return leafNode;
	}
	public void setLeafNode(boolean leafNode) {
		this.leafNode = leafNode;
	}
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
	}
	public int[] getClassCount() {
		return classCount;
	}
	public void setClassCount(int[] classCount) {
		this.classCount = classCount;
	}
	public SubTree getSubTree() {
		return subTree;
	}
	public void setSubTree(SubTree subTree) {
		this.subTree = subTree;
	}

	// this method is called recursively to make the tree on the training data
	public void makeTree(Instances data){
		int classValues = countClassValues(data);
		//System.out.println("classValues : "+classValues);
		classCount = new int[classValues];
		
		// here we count each (result) class's values
		for(int i=0;i<data.numInstances();i++){
			int classVal = (int)data.instance(i).classValue();
			//System.out.println(classVal);
			//System.out.println(data.instance(i));
			classCount[classVal] = classCount[classVal]+1;
		}
		
		/*for(int i=0;i<classCount.length;i++){
			System.out.println("classCount["+i+"] : "+classCount[i]);
		}*/
		
		
		// now we check if the node is a pure node or 
		// if the no. of instances in the node is less than classValues
		int classVariations = numClasses(data.numInstances());
		if(classVariations==1 || data.numInstances()<classValues){
			// if it is a pure node we mark it
			leafNode = true;
			int max = 0;
			for(int i=0;i<classCount.length;i++){
				if(classCount[i]>max){
					max = classValues;
					result = i;
				}
			}
			return;
		} else{
			
			// in case node is not pure we would find the attribute with max
			// info gain and work recursively
			float infoGainMax = -99999; // very small number
			for(int i=0;i<data.numAttributes()-1;i++){
				SubTree subTreeTemp = new SubTree(data,i);
				float infoGain = subTreeTemp.getInfoGain();
				//System.out.println("infoGain :"+infoGain);
				if(infoGain>infoGainMax && subTreeTemp.erroPruning()){
					infoGainMax = infoGain;
					subTree = subTreeTemp;
				}
			}
			
			if(subTree==null){
				// if the subtree works out to be null then the node is
				// pure leaf node
				leafNode = true;
				int max = 0;
				for(int i=0;i<classCount.length;i++){
					if(classCount[i]>max){
						max = classValues;
						result = i;
					}
				}
				return;
			}
			
			// based on the attribute with max info gain we split the tree
			// into more parts - based on the attribute type
			Instances[] subTrees = this.subTree.splitIntoSubTrees(data);
			
			//System.out.println("subTrees.len : "+subTrees.length);
			/*for(int i=0;i<subTrees.length;i++){
				System.out.println("subTrees[i].len : "+subTrees[i].size());
			}*/
			
			childNodes =  new Node[subTrees.length];
			//System.out.println("childNodes.siz : "+childNodes.length);
			for(int i=0;i<childNodes.length;i++){
				childNodes[i] = new Node();
				// make recursive calls till reach pure node
				childNodes[i].makeTree(subTrees[i]);
			}
		}
	}

	// method to calculate the class variations in a subtree
	private int numClasses(int numInstances) {
		//System.out.println("numInstances : "+numInstances);
		for(int i=0;i<classCount.length;i++){
			if(classCount[i]==numInstances){
				//System.out.println("is it ? ");
				return 1;
			}
		}
		return 0;
	}

	// method to count the number of class values
	public int countClassValues(Instances data) {
		return data.classAttribute().numValues();
	}
	
	// method to determine the instance's result class
	// as deciphering from the tree
	public int findClass(Instance inst){
		if(leafNode == true){
			return result;
		}
		
		int childIndex = subTree.getInstanceState(inst);
		Node sub = childNodes[childIndex];
		// if the node is not pure we need to recursively work 
		// till we get the result class
		return sub.findClass(inst);
		
	}
}
