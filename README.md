Team No. : 12
Team Name : The Raging Sharks
Members:
Anjal Patni
Jialu Yuan
Monica Khuswaha
Aayush Chandra

ID3 Decision Tree
We have implemented ID3 decision tree in Java. The project loads the dataset into the program, creates decision tree and shows the cross validation results. 

Setup:
We have made a properties file which contains the name of the training data set and test data set.
Properties file : ID3DecisionTree.properties
By default it has been set to run on Problem A dataset and test set.
By changing the name of the parameters in the file we can make it run for Part B.
( comment the two lines and uncomment next two lines)

We have made a script file which compiles all the java files and shows the cross validation results and the results for test dataset.
Script file : runID3Team12.sh
It uses weka jar which has been provided with the code.

Running:
Just run the script file runID3Team12.sh.
It display the cross validation results and the result for test dataset.
By changing the name of files in the properties file we can change the training and test dataset.


References:
http://en.wikipedia.org/wiki/ID3_algorithm
http://en.wikipedia.org/wiki/Decision_tree_learning
https://github.com/Steve525/decision-tree/blob/master/decisiontree/ID3.java
http://weka.sourceforge.net/doc.dev/weka/core/converters/ConverterUtils.DataSource.html
http://weka.sourceforge.net/doc.dev/weka/core/Instances.html#resampleWithWeights(java.util.Random,%20boolean[])
http://weka.sourceforge.net/doc.dev/weka/core/Instance.html#classIndex()
http://weka.sourceforge.net/doc.stable/
http://www.cs.waikato.ac.nz/ml/weka/documentation.html
http://grepcode.com/file/repository.pentaho.org/artifactory/pentaho/pentaho.weka/pdm/3-7-7/weka/core/Javadoc.java
https://ianma.wordpress.com/2010/01/16/weka-with-java-eclipse-getting-started/
https://github.com/Steve525/decision-tree/blob/master/decisiontree/ID3.java
